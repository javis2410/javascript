var calcularScore = function porcentaje(nombre, buenas, malas) {
    var p = (buenas / 100) * 100;
    var n = (malas / 100) * 100;
    var s = '';
    if (p > 90)
        s = 'A';
    else if (p >= 70)
        s = 'B';
    else if (p >= 45)
        s = 'C';
    else
        s = 'D';
    return `${nombre} tiene el score: ${s}, positivas: ${p}, negativas: ${n}`;
}
var resultado = calcularScore('Pablo', 75, 25);
console.log(resultado);