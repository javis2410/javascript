//Examinando el DOM
//console.dir(document)
/**console.log(document.title);
document.title = 'Prueba';
console.log(document.title);
console.log(document.head);
console.log(document.body);
//console.log(document.all);
console.log(document.all[6]);
console.log(document.forms[0]);
console.log(document.links);*/

//getElementById()
//console.log(document.getElementById('header-title'));
/**var headertitle = document.getElementById('header-title');
var header = document.getElementById('main-header');
//console.log(header);
headertitle.textContent = 'Hola Perros';
headertitle.innerText = 'Adios';
header.innerHTML='<h3> Prueba </h3>';*/

//getElementByClassName()
/**var items=document.getElementsByClassName('list-group-item');
console.log(items[3]);
items[0].textContent='Prueba';*/

//getElementsByTagName
/**var items = document.getElementsByTagName('li');
items[0].textContent='Prueba';*/

//querySelector

/**var input = document.querySelector('input');
input.value = 'Hola Mundo';

var boton = document.querySelector('input[type=submit]');

var item = document.querySelector('.list-group-item');
item.style.color = 'red';
boton.value='Enviar';*/


//querySelectorAll
/** var impar = document.querySelectorAll('li:nth-child(odd)');
var par = document.querySelectorAll('li:nth-child(even)');
for (var i = 0; i < impar.length; i++){
    impar[i].style.backgroundColor = '#CCC';
    par[i].style.backgroundColor='#f4f4f4';
}
*/

//ParentNode
/**var itemList=document.querySelector('#items');
console.log(itemList.parentNode);
var main=itemList.parentNode;
main.style.backgroundColor='#f4f4f4';
console.log(main.parentNode);
*/


//ParentElement
/**var itemList=document.querySelector('#items');
console.log(itemList.parentElement);
var main=itemList.parentElement;
main.style.backgroundColor='#f4f4f4';
console.log(main.parentElement);
*/

//var itemList=document.querySelector('#items');
//childNodes
//console.log(itemList.childNodes);

//children
//console.log(itemList.children);


//firstChild / firstElementChild
//console.log(itemList.firstChild);
//console.log(itemList.firstElementChild);
//itemList.firstElementChild.textContent='Pruebas';


//lastChild / lastElementChild
//console.log(itemList.lastChild);
//console.log(itemList.lastElementChild);
//itemList.lastElementChild.textContent='Es el útlimo';

/** 
var itemList=document.querySelector('#items');
 //previousSibling
console.log(itemList.previousSibling);

 //previousElementSibling
 console.log(itemList.previousElementSibling);

 //nextSibling
 console.log(itemList.nextSibling);

 //nextElementSibling
 console.log(itemList.nextElementSibling);
 */

//createElement
//tagName
/**var nuevoDiv=document.createElement('div');
nuevoDiv.className='hola';
nuevoDiv.id='div-hola';
nuevoDiv.setAttribute('title','Hola mundo');



//createTextNode
var nuevoNodoText=document.createTextNode('Hola mundo');
nuevoDiv.appendChild(nuevoNodoText);

console.log(nuevoDiv);

var contenedor= document.querySelector('header .container');
console.log(contenedor);

var h1=document.querySelector('h1');
console.log(h1);

contenedor.insertBefore(nuevoDiv,h1);
*/

//Agregar Eventos
//document.getElementById('boton').addEventListener('click',function(){
//   console.log('Usted hizo click2');
//});

/**document.getElementById('boton').addEventListener('click',hacerClick);


function hacerClick() {
    //console.log('Usted hizo click....');
    document.getElementById('header-title').textContent='texto cambiado';
}*/

//Agregar un elemento a la lista

var form = document.getElementById('formAgregar');
var lista = document.getElementById('items');
var filtro = document.getElementById('filtro');

//Evento del teclado en el campo de filtro
filtro.addEventListener('keyup', filtarItems);

//Evento submit del formulario
form.addEventListener('submit', agregarItem);

//Evento click de la lista
lista.addEventListener('click', eliminarItem);

//funcion para agregar un item a la lista
function agregarItem(e) {
    e.preventDefault();
    var newItem = document.getElementById('item').value;

    var li = document.createElement('li');
    li.className = 'list-group-item';
    li.appendChild(document.createTextNode(newItem));

    var botonDel = document.createElement('button');
    botonDel.className = 'btn btn-danger btn-sm float-right';
    botonDel.appendChild(document.createTextNode('x'));

    li.appendChild(botonDel);


    lista.appendChild(li);
}

//función para eliminar item de la lista
function eliminarItem(e) {
    if (e.target.classList.contains('eliminar')) {
        if (confirm('¿Seguro que desea eliminar el elemeno?')) {
            var li = e.target.parentElement;
            lista.removeChild(li);
        }
    }
}

//Buscar elemento
function filtarItems(e) {
    var texto = e.target.value.toLowerCase();
    var items = lista.getElementsByTagName('li');
    Array.from(items).forEach(function (item) {
        var itemNombre = item.firstChild.textContent;
        if (itemNombre.toLowerCase().indexOf(texto) != -1)
            item.style.display = 'block';
        else
            item.style.display = 'none';
    });
}