//Manejo de fecha y hora
// const ahora = new Date();
// const timeStamp=ahora.getTime();
// console.log(timeStamp.toString());

// const fechaActual=new Date(timeStamp);
// console.log(fechaActual.getFullYear());

// const fecha1 = new Date("Decembre 17, 1995 06:20:10");
// console.log(ahora.toString());
// console.log(fecha1.toString());

// console.log(`Año: ${ahora.getFullYear()}`);
// console.log(`Mes: ${ahora.getMonth()}`);
// console.log(`Día: ${ahora.getDate()}`);
// console.log(`Día: ${ahora.getDay()}`);

// console.log(`Hora: ${fecha1.getHours()}`);
// console.log(`Minutos: ${fecha1.getMinutes()}`);
// console.log(`Segundos: ${fecha1.getSeconds()}`);


const fechaCualquiera = new Date('April 01 2017 12:00:00');
const fechaActual = new Date();

const timeStampFechaCualquiera = fechaCualquiera.getTime();
const timeStampFechaActual = fechaActual.getTime();

if (timeStampFechaCualquiera < timeStampFechaActual)
    console.log(fechaCualquiera.toString());
else if (timeStampFechaActual < timeStampFechaCualquiera)
    console.log(fechaActual.toString());
