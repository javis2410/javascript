//ES5- variables
//var nombre5='Pablo';
//var edad5=35;
//nombre5='Carlos';
//console.log(nombre5);

//ES6 - variables
//const nombre6='Pedro';
//let edad6=28;
//console.log(edad6);
//nombre6='Ana';
//edad6=30;
//console.log(edad6);

//Bloques y alcance de las variables

//{
//const a = 3;
//let b = 4;
//var c = 5;
//}
//console.log(a + b);
//console.log(c);

//Template Strings

// let nombre = 'Pablo';
// let apellido = 'Vásquez';
//const ciudad = 'Lima';
//const nacimiento = 1985;

//function calcularEdad(year) {
//   return 2020 - year;
//}

//ES5
//console.log(nombre + ' ' + apellido + ' ,nació en: ' + ciudad + ', y su //edad es:' + calcularEdad(nacimiento));

//ES6
//console.log(`${nombre} ${apellido} ,nacio en: ${ciudad}, y su edad es: $//{calcularEdad(nacimiento)}`);

//Funciones de cadenas ES6
/**let nombreCompleto=`${nombre} ${apellido}`;
console.log(nombreCompleto);
console.log(`${nombre} `.repeat(5));
console.log(nombreCompleto.includes('blo'));
console.log(nombreCompleto.startsWith('Pe'));
console.log(nombreCompleto.endsWith('uez'));*/

//Funciones Flecha
// const years = [2000, 2005, 2008, 2012];


//ES5
// var edad5 = years.map(function (el) {
//     return 2020 - el;
// });
// console.log(edad5);

// //ES6
// let edad6 = years.map(el => 2020 - el);
// console.log(edad6);

// edad6 = years.map((el, index) => `edad ${index + 1}: ${2020 - el}`);
// console.log(edad6);

// edad6 = years.map(
//     (el, index) => {
//         const yearActual = new Date().getFullYear();
//         const edad = yearActual - el;
//         return `Edad ${index + 1}: ${edad}`;
//     }
// );
// console.log(edad6);

// const cuadrado = (numero) => numero * numero;
// console.log(cuadrado(2));

// const personas = [
//     {
//         nombre: 'Pablo',
//         edad: 20
//     }, {
//         nombre: 'Ana',
//         edad: 25
//     }, {
//         nombre: 'Pablo',
//         edad: 30
//     },{
//         nombre: 'Pepe',
//         edad: 35
//     }
// ];

// const menores30 = personas.filter(function (personas) {
//     return personas.edad < 35;
// });

// const menores30 = personas.filter((personas)=>personas.edad < 30);

// console.log(menores30);

// Destructuración - Destrucción - Destructuring

//ES5
//var datos = ['Pablo', 25];

// var nombre=datos[0];
// var edad=datos[1];


//ES6
// var [nombre, edad] = ['Pablo', 25];
// console.log(nombre);
// console.log(edad);

// const persona = {
//     Nombre: 'Carlos',
//     Edad: 30
// }

// const { Nombre, Edad } = persona;
// console.log(Nombre);
// console.log(Edad);

// const { Nombre: n, Edad: e } = persona;
// console.log(n);
// console.log(e);

// const calcularMayoriaEdad = (year) => {
//     const edad = new Date().getFullYear() - year;
//     const mayoria = edad >= 18 ? true : false;
//     return [edad, mayoria];
// }
// const [edad2,mayoria]=calcularMayoriaEdad(2010);
// console.log(edad2);
// console.log(`Es mayor de edad: ${mayoria}`);


//Maps
// const datos = new Map();
// datos.set('nombre', 'Grover');
// datos.set('edad', 30);
// datos.set(1, 'javierwarrio185@gamil.com');
// datos.set(2, 'javi@gmail.com');
// datos.set('movil', 999999);

// console.log(datos.get('nombre'));
// console.log(datos.get(2));
// console.log(datos.size);

// // datos.delete(2);
// // datos.clear();

// datos.forEach((value, key) => {
//     console.log(`${key}: ${value}`);
// });

//Spread Operator

// var suma = function (a, b, c, d) {
//     return a + b + c + d;
// }

// console.log(suma(10, 20, 30, 40));

// var valores = [10, 20, 30, 40];
// const op2 = suma(...valores);
// console.log(op2);

// const rh = ['Pedro', 'Pablo', 'Ana'];
// const contabilidad = ['Felipe', 'Carlos', 'Maria'];

// const empresa=[...rh,...contabilidad];
// console.log(empresa);