/**
 * Objetos Literales
 */
/**
var persona = {
    nombre: 'Pablo',
    apellido: 'Vazquez',
    gustos: ['Futbol', 'Películas', 'Inglés'],
    trabajo: 'Instructor',
    esCasado: true
}
console.log(persona.apellido);
console.log(persona['trabajo']);
persona.esCasado = false;
console.log(persona.esCasado);
*/

/**
 * Objetos con la sintaxis Object
 */

/**
var persona2 = new Object();
persona2.nombre = 'Ana';
persona2.apellido = 'Pinedo';
persona2['tranajo'] = 'WebDeveloper';

console.log(persona2);
*/


var persona = {
    //Propiedades
    nombre: 'Pablo',
    apellido: 'Vazquez',
    gustos: ['Futbol', 'Películas', 'Inglés'],
    trabajo: 'Instructor',
    esCasado: true,
    yearNacimiento:1996,

    //Metodos
    calcularEdad: function () {
        this.edad= 2020-this.yearNacimiento;
    }
};
persona.calcularEdad();
console.log(persona);
