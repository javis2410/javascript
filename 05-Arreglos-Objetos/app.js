/*****************
 * Arrelos en JavaScript
 */

var nombres = ['Panlo', 'Carlos', 'Ana', 'Teresea'];
var vegetales = new Array('Tomate', 'Lechuga', 'Zanahoria');

console.log(nombres[2]);
console.log(vegetales[1]);

console.log(nombres.length);


/**********************************
 * Operaciones con arreglos
 */
var frutas = ['Pera', 'Manzana', 'Uva', 'Sandia'];
console.log(frutas);

frutas.forEach(function (elemento, indice, array) {
    console.log(elemento);
})