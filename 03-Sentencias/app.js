/**var nombre = 'Pablo';
var edad = 4;
//Edad <12 es un niño
//Edad >11 y <18 es un adolescente
//Edad >17 y <60 es un adulto
//Edad >60, es un anciano

if (edad < 12)
    console.log(nombre + ' es un niño');
else if (edad > 11 && edad < 18)
    console.log(nombre + ' es un adolescente');
else if (edad > 17 && edad < 60)
    console.log(nombre + ' es un adulto');
else
    console.log(nombre + ' es un anciano');
var nombre='Pablo';
var edad=15;
console.log(edad>=18 ? nombre+' es mayor de edad': nombre+' es menor de edad');


var mes = 9;
switch (mes) {
    case 1:
        console.log('Enero');
        break;
    case 2:
        console.log('Febrero');
        break;
    case 3:
        console.log('Marzo');
        break;
    case 4:
        console.log('Abril');
        break;
    default:
        console.log('Mes no considerado');
        break;
}

for (var i = 10; i > 0; i--)
    console.log(i);

var i = 0;
while (i <= 10) {
    console.log(i)
    i++;
}


var i = 1;
do {
    console.log(i)
    i++
}
while (i <= 10)
{
    console.log(i);
    i++;
}
**/

var edad;
edad = 10;
if (edad)
    console.log('Variable esta definida:');
else
    console.log('Variable no definida');

if (edad === '10')
    console.log('Variable con coersión');
else
    console.log('Variable sin coersión');