// let miLibroA = {
//     titulo: 'El libro de JavaScript',
//     autor: 'Javier Guerrero',
//     paginas: 400,
//     publicado: false
// }

// let miLibrB = {
//     titulo: 'Programación en PHP',
//     autor: 'Javier Guerrero',
//     paginas: 700,
//     publicado: true
// }

// console.log(miLibroA.publicado);
// console.log(`${miLibroA.titulo} creado por ${miLibroA.autor}`);

// miLibroA.paginas = 500;
// console.log(miLibroA.paginas);

// let getResumen = (Libro) => {
//     return {
//         resumen: `${Libro.titulo} creado por ${Libro.autor}`,
//         resumenPaginas: `${Libro.titulo} tiene ${Libro.paginas} paginas.`
//     }
// }

// let libroAResumen=getResumen(miLibroA);
// let libroBResumen=getResumen(miLibrB);

// console.log(libroBResumen.resumen);
// console.log(libroBResumen.resumenPaginas);

// console.log(libroAResumen.resumen);
// console.log(libroAResumen.resumenPaginas);

// let persona = {
//     nombre: 'Javier',
//     edad: 24,
//     ciudad: 'México'
// }
// persona.edad = 23;
// console.log(`${persona.nombre} tiene: ${persona.edad} años, y vive en la ciudad de: ${persona.ciudad}`);


//Referencia de objetos

// let persona = {
//     nombre: 'Pablo',
//     edad: 30,
//     sueldo: 1200
// }

// let otraPersona=persona;
// otraPersona.sueldo=1500;
// console.log(otraPersona);

// let cambiarSueldo = (p, monto) => {
//     p.sueldo = p.sueldo + monto;
//     //console.log(p);
// }
// cambiarSueldo(persona, 500);
// console.log(otraPersona);
// console.log(persona);

//Métodos
// let persona = {
//     nombre: 'Pablo',
//     edad: 30,
//     sueldo: 1200,
//     metodoPrueba: function () {
//         //console.log('Escribiendo desde el método Prueba');
//         return 'Resultado desde Prueba';
//     },
//     cambiarEdad: function(e){
//         this.edad=this.edad+e;
//     }
// }
// let result = persona.metodoPrueba();
// console.log(result);
// persona.cambiarEdad(3);
// console.log(persona.edad);

//Objeto String
// let nombre = ' Javier Guerrero ';
// let clave = '12309clave567.e';
// console.log(nombre.length);
// console.log(nombre.toUpperCase());
// console.log(nombre.toLowerCase());
// console.log(nombre.trim());
// console.log(clave.includes('12309clave567.e'));


//Destructuración de objetos

const getPersonaDatos = () => {
    const respuesta = [
        {
            codigo: 200,
            data: {
                persona: {
                    nombre: 'Pablo',
                    direccion: {
                        ciudad: 'Lima',
                        pais: 'Perú'
                    }
                }
            }
        }, {
            codigo: 300,
            data: {
                persona: {
                    nombre: 'Ana',
                    direccion: {
                        ciudad: 'Santiago',
                        pais: 'Chile'
                    }
                }
            }
        }, {
            codigo: 400,
            data: {
                persona: {
                    nombre: 'José',
                    direccion: {
                        ciudad: 'Bogota',
                        pais: 'Colombia'
                    }
                }
            }
        }
    ]
    return respuesta;

}


let { codigo: status, data: { persona: { nombre: nom } } } = getPersonaDatos()[0];
for (const { codigo: status, data: { persona: { nombre: n } } } of getPersonaDatos())
    console.log(n)




